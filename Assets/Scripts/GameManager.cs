﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    [HideInInspector] public bool isGameOver = false;

    public int CurrentFishCount {
        get { return currentFishCount; }
    }

    public float CurrentTime {
        get { return currentTime; }
    }

    float currentTime = 0;
    bool isTimeCount = false;
    int poiDestroyCount = 0;
    int currentFishCount;

    void Awake() {
        Instance = this;
    }

    void Start() {
        isTimeCount = true;
    }

    void Update() {
        if (isTimeCount) {
            currentTime += Time.deltaTime;
        }

        UIManager.Instance.SetCurrentTime(currentTime);
    }

    public void AddPoiDestroyCount() {
        poiDestroyCount++;
        UIManager.Instance.SetPoiDestroyCount(poiDestroyCount);
    }

    public void AddGoldFishCount() {
        currentFishCount++;
    }

    public void DecrementGoldFishCount() {
        if(currentFishCount > 0) {
            currentFishCount--;
            if(currentFishCount == 0) {
                GameOver();
            }
        }
    }

    public void GameOver() {
        isTimeCount = false;
        isGameOver = true;
        UIManager.Instance.SetGameOverText(true);
        StartCoroutine(ResultCoroutine());
    }

    IEnumerator ResultCoroutine() {
        yield return new WaitForSeconds(1.0f);
        UIManager.Instance.SetResultText(currentTime, poiDestroyCount);
    }

}

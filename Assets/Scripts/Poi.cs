﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poi : MonoBehaviour {

    [HideInInspector] public Rigidbody rigidbody;

    public bool canMove {
        get { return (!isCatching && defaultObject.activeInHierarchy); }
    }

    [SerializeField] int durability;
    [SerializeField] GameObject defaultObject;
    [SerializeField] GameObject breakObject;
    [SerializeField] Collider trigger;
    [SerializeField] int ignoreRaycastLayer = 2;
    [SerializeField] AudioSource releaseAudioSource;
    [SerializeField] AudioSource breakAudioSource;
    [Space]
    [SerializeField] float speed;
    [SerializeField] float catchTime;
    [SerializeField] float poiLifeTime;

    List<Goldfish> goldfishList = new List<Goldfish>();
    Vector3 targetPosition;
    bool isCatching = false;
    bool isPullingUp = false;
    Vector3 pullingUpPosition;
    float currentTime = 0;

    // Use this for initialization
    void Start () {
        //trigger.enabled = true;
        trigger.gameObject.layer = ignoreRaycastLayer;
        rigidbody = GetComponent<Rigidbody>();
        PoiController.Instance.AddPoi(this);
        StartCoroutine(CatchCoroutine());
        StartCoroutine(RotateCoroutine());
        //StartCoroutine(DestroyCoroutine());
	}
	
	// Update is called once per frame
	void Update () {

        //if (!isCatching && defaultObject.activeInHierarchy) {
        //    Move();
        //} else {
        //    OnCatching();
        //}

        if (!canMove) {
            OnCatching();
        }

        if (isPullingUp) {
            transform.position = Vector3.Lerp(transform.position, pullingUpPosition, Time.deltaTime);
        }

    }

    void OnDestroy() {
        PoiController.Instance.RemovePoi(this);
        Release();   
    }

    public void Move() {
        Vector3 f = (targetPosition - transform.position).normalized;
        if (Vector3.Distance(transform.position, targetPosition) >= 0.01f) {
            f *= speed;
        } else {
            targetPosition = new Vector3(Random.Range(-1.6f, 1.6f), 0.4f, Random.Range(0.4f, 0.8f));
        }
        rigidbody.velocity = f;

    }

    void OnCatching() {
        rigidbody.velocity = Vector3.zero;
        if (Input.GetButtonDown("Fire1")) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                if (hit.transform.root.gameObject == this.gameObject) {
                    for(int i = 0; i < goldfishList.Count; i++) {
                        goldfishList[i].Struggle();
                    }
                }
            }
        }
    }

    IEnumerator RotateCoroutine() {
        while (true) {
            targetPosition = new Vector3(Random.Range(-1.6f, 1.6f), 0.4f, Random.Range(0.4f, 0.8f));
            yield return new WaitForSeconds(Random.Range(1f, 3.0f));
        }
    }

    IEnumerator CatchCoroutine() {
        while (true) {
            yield return new WaitForSeconds(0.5f);

            if (!isCatching && defaultObject.activeInHierarchy && goldfishList.Count != 0) {
                Catch();
                while(currentTime <= catchTime) {
                    currentTime += Time.deltaTime;
                    yield return null;
                }
                foreach(Goldfish fish in goldfishList) {
                    Destroy(fish.gameObject, 0.5f);
                    fish.Dead();
                    fish.transform.parent = transform;
                }
                pullingUpPosition = transform.position;
                pullingUpPosition.z = 2.0f;
                isPullingUp = true;
                goldfishList.Clear();
                Destroy(gameObject, 0.5f);
                yield break;
            }
        }
    }

    IEnumerator DestroyCoroutine() {
        yield return new WaitForSeconds(poiLifeTime);
        if (!isCatching) {
            Destroy(gameObject);
        }
    }

    public void AddDamage() {
        if (durability != 0) {
            durability--;
            if(durability == 0) {
                defaultObject.SetActive(false);
                breakObject.SetActive(true);
                GameManager.Instance.AddPoiDestroyCount();
                releaseAudioSource.Play();
                breakAudioSource.Play();
                Release();
                Destroy(gameObject, 1.0f);
            }
        }
    }

    void Catch() {
        //trigger.enabled = false;
        //trigger.gameObject.layer = ignoreRaycastLayer;
        trigger.gameObject.layer = 0;
        isCatching = true;
        transform.Translate(0.0f, 0.1f, 0.0f);
        foreach(Goldfish fish in goldfishList) {
            fish.Catch(this);
        }
    }

    void Release() {
        foreach (Goldfish fish in goldfishList) {
            fish.Release();
        }
        //trigger.enabled = false;
        trigger.gameObject.layer = ignoreRaycastLayer;
        //isCatching = false;
        goldfishList.Clear();
    }

    void OnTriggerEnter(Collider other) { 
        Goldfish fish = other.transform.root.GetComponent<Goldfish>();
        if(fish != null && !isCatching) {
            if (!goldfishList.Contains(fish) && !fish.isTargeted) {
                goldfishList.Add(fish);
                fish.isTargeted = true;
            }
        }
    }

    void OnTriggerExit(Collider other) {
        Goldfish fish = other.transform.root.GetComponent<Goldfish>();
        if (fish != null && !isCatching) {
            fish.isTargeted = false;
            goldfishList.Remove(fish);
        }
    }
}

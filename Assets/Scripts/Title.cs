﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title : MonoBehaviour {

    [SerializeField] RectTransform howToPlayPanel;
    [SerializeField] RectTransform contents;
    [SerializeField] ScrollRect scrollRect;

    bool isOpenHowToPlay = false;
    int howToPlayIndex = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateHowToPlay();
	}

    void UpdateHowToPlay() {
        if (isOpenHowToPlay) {
            scrollRect.horizontalNormalizedPosition =
                Mathf.Lerp(
                scrollRect.horizontalNormalizedPosition, 1,
                Time.deltaTime * 10f);
        } else {
            scrollRect.horizontalNormalizedPosition = 0f;
        }

        scrollRect.transform.localScale =
            Vector3.Lerp(
                scrollRect.transform.localScale,
                isOpenHowToPlay ? Vector3.one : Vector3.zero,
                Time.deltaTime * 10f);
    }

    public void MoveNextPage() {
        if (isOpenHowToPlay) {
            howToPlayIndex++;
            if (howToPlayIndex >= contents.childCount) {
                CloseHowToPlay();
                return;
            }
            contents.GetChild(howToPlayIndex).gameObject.SetActive(true);
        }
    }

    public void OpenHowToPlay() {
        if (!isOpenHowToPlay) {
            isOpenHowToPlay = true;
            contents.GetChild(0).gameObject.SetActive(true);
            contents.GetChild(contents.childCount - 1).gameObject.SetActive(false);
        }
    }

    void CloseHowToPlay() {
        howToPlayIndex = 0;
        for (int i = 0; i < contents.childCount - 1; i++) {
            contents.GetChild(i).gameObject.SetActive(false);
        }
        isOpenHowToPlay = false;
        scrollRect.horizontalNormalizedPosition = 0f;
    }

    public void SceneChangeTest() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldFishSpawner : MonoBehaviour {

    [SerializeField] GameObject goldFishPrefab;
    [SerializeField] float spawnTime;
    [SerializeField] int goldfishCount;

    List<Goldfish> goldfishList = new List<Goldfish>();

    void Start() {
        StartCoroutine(SpawnGoldfishCoroutine());
    }

    public void AddGoldfish(Goldfish fish) {
        if (!goldfishList.Contains(fish)) {
            goldfishList.Add(fish);
        }
    }

    public void RemoveGoldfish(Goldfish fish) {
        goldfishList.Remove(fish);
    }

    public void ChangePlayer() {
        if (goldfishList.Count > 0) {
            goldfishList[0].tag = "Player";
        }
    }

    IEnumerator SpawnGoldfishCoroutine() {
        while (!GameManager.Instance.isGameOver) {
            if (GameManager.Instance.CurrentFishCount < goldfishCount && !CheckChatched()) {
                GameObject go = Instantiate(goldFishPrefab) as GameObject;
                Vector3 pos = new Vector3(Random.Range(-1.6f, 1.6f), 0.4f, Random.Range(0.4f, 0.8f));
                go.transform.position = pos;
                go.transform.rotation = Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f);
            } else {
                //yield break;
            }
            yield return new WaitForSeconds(Random.Range(spawnTime, spawnTime + 2.0f));
        }
    }

    bool CheckChatched() {
        foreach(Goldfish fish in goldfishList) {
            if (!fish.CanMove) {
                return true;
            }
        }
        return false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    [SerializeField] RectTransform onGamePanel;
    [SerializeField] Text timeText;
    [SerializeField] Text poiDestroyCountText;
    [SerializeField] Text gameOverText;
    [Space]
    [SerializeField] RectTransform resultPanel;
    [SerializeField] Text resultText;

    Vector3 countTextScale;
    float resultTime;
    int resultCount;

    void Awake() {
        Instance = this;
        countTextScale = poiDestroyCountText.transform.localScale;
    }

    void Update() {
        poiDestroyCountText.transform.localScale = Vector3.Lerp(poiDestroyCountText.transform.localScale, countTextScale, 5.0f * Time.deltaTime);
    }

    public void SetCurrentTime(float currentTime) {
        int millisecond = (int)Mathf.Floor((currentTime - Mathf.Floor(currentTime)) * 100);
        timeText.text = System.String.Format("生存時間 {0:000}.{1:00}", (int)currentTime, millisecond);
    }

    public void SetPoiDestroyCount(int count) {
        poiDestroyCountText.text = "破壊したポイ " + count.ToString();
        poiDestroyCountText.transform.localScale = Vector3.one * 1.1f;
    }

    public void SetGameOverText(bool enable) {
        if(gameOverText == null) {
            return;
        }
        gameOverText.gameObject.SetActive(enable);
    }

    public void SetResultText(float time, int count) {
        resultTime = time;
        resultCount = count;
        onGamePanel.gameObject.SetActive(false);
        resultPanel.gameObject.SetActive(true);
        int millisecond = (int)Mathf.Floor((time - Mathf.Floor(time)) * 100);
        resultText.text = string.Format("あなたの金魚は\n{0}.{1}秒生き残り\n{2}枚のポイを破壊しました", (int)time, millisecond, count);

    }

    public void GotoTitle() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Title");
    }

    public void SendRanking() {
        //naichilab.RankingLoader.Instance.SendScoreAndShowRanking((int)resultTime);
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking((double)resultTime);
    }

    public void TweetResult() {
                int millisecond = (int)Mathf.Floor((resultTime - Mathf.Floor(resultTime)) * 100);
        string text = string.Format("あなたの金魚は{0}.{1}秒生き残り{2}枚のポイを破壊しました\n#unity1week #unityroom #反逆の金魚\n", 
                                    (int)resultTime, millisecond, resultCount);
        text += "https://unityroom.com/games/rebellion_of_the_goldfish";
        string url = "http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(text);
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            Application.ExternalEval("window.open(\"" + url + "\",\"_blank\")");
            return;
        }
        Application.OpenURL(url);
    }

}

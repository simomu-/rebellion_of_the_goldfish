﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Goldfish : MonoBehaviour {

    [HideInInspector] public Rigidbody rigidbody;
    [HideInInspector] public bool CanMove = true;
    [HideInInspector] public bool isTargeted = false;
    [HideInInspector] public bool isLive = true;

    [SerializeField] float speed;

    Vector3 targetPosition;
    bool isGroupe = false;
    bool bounce;
    Poi currentPoi;
    Animator animator;

	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        if(this.tag != "Player") {
            StartCoroutine(RotateCoroutine());
        }
        GameManager.Instance.AddGoldFishCount();
        FindObjectOfType<GoldFishSpawner>().AddGoldfish(this);
	}
	
	void Update () {
        if (CanMove) {
            Move();
        } else {
        }
	}

    void OnDestroy() {

    }

    void Move() {
        if (this.tag == "Player") {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Vector3 f = Vector3.zero;
            Vector3 pos = transform.position;
            if (Physics.Raycast(ray, out hit)) {
                pos = hit.point;
                pos.y = 0.4f;
                f = (pos - transform.position).normalized;
            }
            if (Vector3.Distance(transform.position, pos) >= 0.01f) {
                rigidbody.velocity = f * speed;
            } else {
                rigidbody.velocity = Vector3.zero;
            }
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                      Quaternion.LookRotation((pos - transform.position).normalized),
                                      Time.deltaTime * 10f);

            GoldfishGroupController.Instance.targetPosition = transform.position;
        } else {
            if (!isGroupe) {
                Vector3 f = (targetPosition - transform.position).normalized;
                if (Vector3.Distance(transform.position, targetPosition) >= 0.01f) {
                    f *= speed;
                } else {
                    targetPosition = new Vector3(Random.Range(-1.6f, 1.6f), 0.4f, Random.Range(0.4f, 0.8f));
                }
                rigidbody.velocity = f;
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                          Quaternion.LookRotation((targetPosition - transform.position).normalized),
                                          Time.deltaTime * 10f);
            }
        }
    }

    public void Catch(Poi poi) {
        currentPoi = poi;
        CanMove = false;
        GetComponent<Collider>().enabled = false;
        rigidbody.velocity = Vector3.zero;
        Vector3 pos = transform.position;
        pos.y = 0.6f;
        transform.position = pos;
        transform.Rotate(0, 0, 90f);
        transform.Rotate(0.0f, 10.0f, 0.0f);
        GoldfishGroupController.Instance.RemoveGoldfish(this);
    }

    public void Struggle() {
        if (Input.GetButtonDown("Fire1") && isLive) {
            currentPoi.AddDamage();
            animator.SetTrigger("Struggle");
            //if (bounce) {
            //    transform.Rotate(0.0f, -10.0f, 0.0f);
            //    bounce = !bounce;
            //} else {
            //    transform.Rotate(0.0f, 10.0f, 0.0f);
            //    bounce = !bounce;
            //}
        }
    }

    public void Release() {
        currentPoi = null;
        CanMove = true;
        GetComponent<Collider>().enabled = true;
        transform.rotation = Quaternion.identity;
        Vector3 pos = transform.position;
        pos.y = 0.4f;
        transform.position = pos;
        if(this.tag != "Player") {
            GoldfishGroupController.Instance.AddGoldFish(this);
        }
    }

    public void Dead() {
        isLive = false;
        GoldfishGroupController.Instance.RemoveGoldfish(this);
        GoldFishSpawner s = FindObjectOfType<GoldFishSpawner>();
        if (s != null) {
            s.RemoveGoldfish(this);
        }
        GameManager.Instance.DecrementGoldFishCount();
        if (this.tag == "Player") {
            if (s != null) {
                s.ChangePlayer();
            }
        }
    }

    void OnTriggerEnter(Collider _other) {
        if (_other.transform.tag == "GameController") {
            transform.GetChild(0).gameObject.SetActive(true);
        }

        if(_other.transform.root.tag == "Player" && !isGroupe) {
            isGroupe = true;
            GoldfishGroupController.Instance.AddGoldFish(this);
            rigidbody.velocity = Vector3.zero;
            StopAllCoroutines();
        }
    }

    void OnTriggerExit(Collider _other) {
        if (_other.transform.root.tag == "GameController") {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    IEnumerator RotateCoroutine() {
        while (true) {
            targetPosition = new Vector3(Random.Range(-1.6f, 1.6f), 0.4f, Random.Range(0.4f, 0.8f));
            yield return new WaitForSeconds(Random.Range(0.3f, 1.0f));
        }
    }
}

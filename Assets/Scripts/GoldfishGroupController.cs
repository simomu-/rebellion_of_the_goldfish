﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldfishGroupController : MonoBehaviour {

    public static GoldfishGroupController Instance;
    public Vector3 targetPosition;
    [SerializeField] GameObject origin;
    [Space]
    [SerializeField] float maxSpeed;
    [SerializeField] float minSpeed;
    [SerializeField, Range(0f, 1.0f)] float turbulence;
    [SerializeField] float distance;

    List<Goldfish> goldfishList = new List<Goldfish>();

    void Awake() {
        Instance = this;
    }

	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        targetPosition = new Vector3(0.0f, 0.4f, 0.5f);
        if (Physics.Raycast(ray, out hit)) {
            Vector3 pos = hit.point;
            pos.y = 0.4f;
            targetPosition = pos;
        }

        //群れの重心算出
        Vector3 center = Vector3.zero;
        for(int i = 0; i < goldfishList.Count; i++) {
            center += goldfishList[i].transform.position;
        }
        center /= (goldfishList.Count);

        center += targetPosition;
        center /= 2.0f;

        //群れを重心に寄らせる
        for (int i = 0; i < goldfishList.Count; i++) {
            Vector3 dirToCenter = (center - goldfishList[i].transform.position).normalized;
            Vector3 direction = (goldfishList[i].rigidbody.velocity.normalized * turbulence + 
                                 dirToCenter * (1 - turbulence)).normalized;

            direction *= Random.Range(minSpeed, maxSpeed);
            goldfishList[i].rigidbody.velocity = direction;
        }

        //群れの間隔を調整する
        for (int i = 0; i < goldfishList.Count; i++) {
            for (int j = 0; j < goldfishList.Count; j++) {
                if (i == j) {
                    continue;
                }
                Vector3 diff = goldfishList[i].transform.position - goldfishList[j].transform.position;
                if (diff.magnitude < Random.Range(0.01f, distance)) {
                    goldfishList[i].rigidbody.velocity = diff.normalized * goldfishList[i].rigidbody.velocity.magnitude;
                }
            }
        }

        Vector3 averageVelocity = Vector3.zero;
        for (int i = 0; i < goldfishList.Count; i++) {
            averageVelocity += goldfishList[i].rigidbody.velocity;
        }
        averageVelocity /= (goldfishList.Count);
        for (int i = 0; i < goldfishList.Count; i++) {
            goldfishList[i].rigidbody.velocity = goldfishList[i].rigidbody.velocity * turbulence + averageVelocity * (1 - turbulence);
            goldfishList[i].transform.rotation = Quaternion.Slerp(goldfishList[i].transform.rotation,
                                                                  Quaternion.LookRotation(goldfishList[i].rigidbody.velocity.normalized),
                                                                  Time.deltaTime * 10f);
        }

        if (GameManager.Instance.isGameOver) {
            StopAllCoroutines();
        }

    }

    public void AddGoldFish(Goldfish fish) {
        if (!goldfishList.Contains(fish)) {
            goldfishList.Add(fish);
        }
    }

    public void RemoveGoldfish(Goldfish fish) {
        goldfishList.Remove(fish);
    }

    public void ChangePlayer() {
        if(goldfishList.Count > 0) {
            goldfishList[0].tag = "Player";
            goldfishList.Remove(goldfishList[0]);
        }
    }
}

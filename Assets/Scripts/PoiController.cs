﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoiController : MonoBehaviour {

    public static PoiController Instance;

    [SerializeField] Poi[] pois;
    [SerializeField] int poiCount;
    [SerializeField] float poiSpawnTime;
    [SerializeField] float levelUpTime;

    List<Poi> poiList = new List<Poi>();
    int level = 0;

    void Start() {
        Instance = this;
        StartCoroutine(PoiSpawnCoroutine());
    }

    void Update() {
        for (int i = 0; i < poiList.Count; i++) {
            for (int j = 0; j < poiList.Count; j++) {
                if (i == j) {
                    if (poiList[i].canMove) {
                        poiList[i].Move();
                    }
                    continue;
                }
                Vector3 diff = poiList[i].transform.position - poiList[j].transform.position;
                if (diff.magnitude < Random.Range(0.3f, 0.5f)) {
                    poiList[i].rigidbody.velocity = diff.normalized * poiList[i].rigidbody.velocity.magnitude;
                } else {
                    if (poiList[i].canMove) {
                        poiList[i].Move();
                    }
                }
            }
        }

        if (GameManager.Instance.CurrentTime >= levelUpTime * (level + 1)) {
            if (pois.Length > 3 + level) {
                level++;
            }
        }

        if (GameManager.Instance.isGameOver) {
            StopAllCoroutines();
        }
        //Debug.Log("current level " + level);
    }

    IEnumerator PoiSpawnCoroutine() {
        while (true) {
            if(poiList.Count < poiCount) {
                Poi p = Instantiate(pois[Random.Range((level + 3) - 3, 3 + level)]) as Poi;
                p.transform.position = new Vector3(Random.Range(-1.6f, 1.6f), 0.4f, Random.Range(0.4f, 0.8f));
            }
            yield return new WaitForSeconds(Random.Range(poiSpawnTime, poiSpawnTime + 2.0f));
        }
    }

    public void AddPoi(Poi poi) {
        if (!poiList.Contains(poi)) {
            poiList.Add(poi);
        }
    }

    public void RemovePoi(Poi poi) {
        poiList.Remove(poi);
    }

}
